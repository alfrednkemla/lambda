package main

import (
	"errors"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
)

func main() {
	lambda.Start(handleRequest)
}

func handleRequest(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	stringResponse := "Method Not OK"
	code := 501
	err := errors.New("Method Not Allowed")
	if request.HTTPMethod == "GET" {
		stringResponse = "Yay a successful response!!"
		code = 200
		err = nil
	}
	APIResponse := events.APIGatewayProxyResponse{Body: stringResponse, StatusCode: code}
	return APIResponse, err
}
